1. Menghapus container

```
docker container rm <nama_container>
docker container rm nginxserver1
```

2. Menghentikan container

```
docker container stop <nama_container>
docker container stop nginxserver1
```

3. Membuat container dengan membuka port

```
docker container create --name=nama_container -p <port-luar>:<port-container> image:tag
docker container create --name=nginxserver1 -p 8080:80 nginx:alpine
```
