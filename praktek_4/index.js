const express = require("express")
const app = express()
const port = 3000

app.get("/", (req, res) => {
  res.json({
    language: "Javascript",
    run_time: "Node JS",
    framework: "express js",
  })
})

app.listen(port, () => {
  console.log(`Run in port ${port}`)
})
