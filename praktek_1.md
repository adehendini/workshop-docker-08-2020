Melihat image yang tersedia

```
docker images
```

Melihat container yang aktif

```
docker container ls
```

Melihat seluruh container baik yang aktif maupun tidak

```
docker container ls --all
```

Praktek pertama (menggunakan image Nginx)

1. Mendownload images Nginx

```
docker pull <nama-image>:<tag>
docker pull nginx:alpine
```

2. Membuat container dari image Nginx

```
docker container create --name nginxserver1 nginx:alpine
```

3. Menjalankan container nginxserver1

```
docker container start nginxserver1
```
