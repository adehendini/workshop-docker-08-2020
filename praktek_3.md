### Membuat image menggunakan Dockerfile

1. Membuat image dari Dockerfile

- masuk ke folder 'praktek_3'

```
docker build --tag nama_aplikasi:versi .
docker build --tag app-php:1.0 .
```

2. Membuat container dari image app-php:1.0

```
docker container create --name app-php1 -p 3030:80 app-php:1.0
```

3. Menjalankan container

```
docker container start app-php1
```
